<?php
// This is a simple script to "fix" DokuWiki page files by replacing
// '[[' and 'http' with "\n[[" and "\nhttp" respectively.

if ($argc < 2) { // if no filenames were input
    showUsageAndExit($argv[0]);
}

// process each file in turn
for ($j = 1; $j < $argc; $j++) {
    processFile($argv[$j]);
}

////// end main program

// Copy a file from $src to $dest preserving permissions and
// timestamps. Return any message; in Unix, a message usually means an
// error occurred.
function filecopy (string $src, string $dest): ?string
{
    $result = shell_exec("cp -p $src $dest");

    return $result;
}

// Load the contents of the file specified by $filename and replace
// any occurrences of '[[' or 'http' with \n[[ or \nhttp respectively.
// If any replacements were made, copy $filename to $filename'.bak'
// and save the file with replaced contents to $filename .
function processFile(string $filename): void
{
    $bakName = $filename . '.bak';

    $changed = false;
    $contents = file_get_contents($filename);
    $originalContents = $contents;

    $contents = str_replace('[[', "\n[[", $contents);

    $contents = str_replace('http', "\nhttp", $contents);

    $changed = ($originalContents != $contents);

    if ($changed) {
        $return = filecopy ($filename, $bakName);
        if (!empty($return)) { // if error message
            exit($return); // display and quit the script
        }
        file_put_contents($filename, $contents);
    }
}

// Describe what this script does and quit the script.
function showUsageAndExit(string $scriptName): void
{
    $msg = "Usage: $scriptName file...\n"
        . "Processes each file... in turn\n"
        . "If the file has 'http' or '[[' *NOT* at the start of the file,\n"
        . "save a copy of the file as filename.bak and write a copy\n"
        . "as filename with a newline just before each 'http' or '[['\n";

    exit($msg);
}
?>

